# Summary

* [Auth](auth/docs.md)
    * [Login](auth/login.md)
    * [Logout](auth/logout.md)

* [Users](users/docs.md)
    * [Create user](users/create.md)
    * [Get users](users/get.md)
    * [Update users](users/update.md)
    * [Delete users](users/delete.md)

* [Teams](teams/docs.md)
    * [Create team](teams/create.md)
    * [Get team](teams/get.md)
    * [Update team](teams/update.md)
    * [Delete team](teams/delete.md)

* [Streams](streams/docs.md)
    * [Create stream](streams/create.md)
    * [Get stream](streams/get.md)
    * [Update stream](streams/update.md)
    * [Delete stream](streams/delete.md)

* [Tasks](tasks/docs.md)
    * [Create task](tasks/create.md)
    * [Get task](tasks/get.md)
    * [Update task](tasks/update.md)
    * [Delete task](tasks/delete.md)
