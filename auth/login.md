# Creating a session

{% method %}
## Login {#post}

Login and create a new session.

{% sample lang="http" %}
### Request
```
POST /api/login
```
| Header        | Description                            |
|---------------|----------------------------------------|
| Authorization | **Required** Basic HTTP authentication |
{% endmethod %}
