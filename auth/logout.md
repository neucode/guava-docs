# Deleting a session

{% method %}
## Logout {#delete}

Invalidate your current session token. You must be logged in.

{% sample lang="http" %}
### Request
```
DELETE /api/logout
```
| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header    | Description                                    |
|-----------|------------------------------------------------|
| Cookie    | **Required** `token` The JWT for authorization |
{% endmethod %}
