# Auth

Guava uses Basic HTTP Authentication to generate a JWT token that can be used
to authenticate other requests. The JWT token can be passed as a cookie, or as
a query parameter. The cookie name, and query name, is `token`.

The JWT token can be passed as a cookie for a query parameter. If marked as
required only a single headers or parameters is required.
