# Getting tasks

{% method %}
## Getting tasks in personal streams {#get}

Get tasks in a personal stream. You must be logged in.

{% sample lang="http" %}
### Request
```
GET /api/streams/:stream/tasks
```
| Parameter | Description                                |
|-----------|--------------------------------------------|
| stream    | **Required** The id or alias of the stream |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
[{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  }
}]
```
{% endmethod %}

{% method %}
## Getting all personal tasks {#get}

Get all personal tasks. You must be logged in.

{% sample lang="http" %}
### Request
```
GET /api/tasks
```
| Parameter | Description                                |
|-----------|--------------------------------------------|
| stream    | **Required** The id or alias of the stream |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
[{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  }
}]
```
{% endmethod %}

{% method %}
## Getting tasks in team streams {#get}

Get your team tasks. You must be a member of the team.

{% sample lang="http" %}
### Request
```
GET /api/teams/:team/stream/:stream/tasks
```
| Parameter | Description                                     |
|-----------|-------------------------------------------------|
| team      | **Required** The id or alias of the owning team |
| stream    | **Required** The id or alias of stream          |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
[{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}]
```
{% endmethod %}

{% method %}
## Getting all tasks in a team {#get}

Get all the tasks in a team. You must be a member of the team.

{% sample lang="http" %}
### Request
```
GET /api/teams/:team/tasks
```
| Parameter | Description                                     |
|-----------|-------------------------------------------------|
| team      | **Required** The id or alias of the owning team |
| stream    | **Required** The id or alias of stream          |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
[{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}]
```
{% endmethod %}

{% method %}
## Getting a personal task {#get}

Get a one of your tasks. You must be logged in.

{% sample lang="http" %}
### Request
```
GET /api/tasks/:task
```
| Parameter | Description                     |
|-----------|---------------------------------|
| task      | **Required** The id of the task |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  }
}
```
{% endmethod %}

{% method %}
## Getting a team task {#get}

Get a one of the team tasks. You must be a member of the team.

{% sample lang="http" %}
### Request
```
GET /api/teams/:team/tasks/:task
```
| Parameter | Description                                     |
|-----------|-------------------------------------------------|
| team      | **Required** The id or alias of the owning team |
| task      | **Required** The id of the task                 |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}
```
{% endmethod %}
