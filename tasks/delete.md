# Deleting tasks

{% method %}
## Deleting tasks in personal streams {#delete}

Delete one of your tasks. You must be logged in.

{% sample lang="http" %}
### Request
```
DELETE /api/tasks/:task
```
| Parameter | Description                                   |
|-----------|-----------------------------------------------|
| task      | **Required** The id of the task being deleted |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
{% endmethod %}

{% method %}
## Deleting tasks in team streams {#post}

Delete one of the team tasks. You must be a member of the team.

{% sample lang="http" %}
### Request
```
DELETE /api/teams/:team/tasks/:task
```
| Parameter | Description                                     |
|-----------|-------------------------------------------------|
| team      | **Required** The id or alias of the owning team |
| task      | **Required** The id of the task being deleted   |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
{% endmethod %}
