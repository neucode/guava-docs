# Updating tasks

{% method %}
## Updating a personal task {#patch}

Update one of your tasks. You must be logged in.

{% sample lang="http" %}
### Request
```
PATCH /api/tasks/:task
```
```
{
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": ["abcd-1234-wxyz-6789"],
  "streams": ["abcd-1234-wxyz-6789"]
}
```
| Parameter | Description                     |
|-----------|---------------------------------|
| task      | **Required** The id of the task |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  }
}
```
{% endmethod %}

{% method %}
## Upating a team task {#patch}

Update one of the team tasks. You must be a member of the team.

{% sample lang="http" %}
### Request
```
PATCH /api/teams/:team/tasks/:task
```
```
{
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": ["abcd-1234-wxyz-6789"],
  "streams": ["abcd-1234-wxyz-6789"]
}
```
| Parameter | Description                                     |
|-----------|-------------------------------------------------|
| team      | **Required** The id or alias of the owning team |
| task      | **Required** The id of the task                 |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}
```
{% endmethod %}
