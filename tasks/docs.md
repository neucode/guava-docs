# Tasks

Tasks are a short description of something that needs to be completed. It can
be grouped into multiple streams and linked to users that are involved.

Only one of `user` or `team` will be available, based on whether the task is 
owned by a user or a team.

```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "description": "Lunch with @loongy #meetings",
  "assignees": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }],
  "streams": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "meetings"
  }],
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  },
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}
```

| Field       | Type   | Description                               |
|-------------|--------|-------------------------------------------|
| created_at  | date   | The date of creation                      |
| updated_at  | date   | The latest date of update                 |
| id          | number | A unique numerical identifier             |
| description | string | A description of the task                 |
| assignees   | array  | All the users involved in this task       |
| streams     | array  | All the streams this task is grouped into |
| user        | object | The owning user                           |
| team        | object | The owning team                           |
