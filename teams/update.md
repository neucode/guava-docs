# Updating teams

{% method %}
## Updating teams {#patch}

Update a team. You must be a member of the team.

{% sample lang="http" %}
### Request
```
PATCH /api/teams/:team
```
```
{
  "id": "abcd-1234-wxyz-6789",
  "alias": "neucode",
  "members": [
    "abcd-1234-wxyz-6789",
    "abcd-1234-wxyz-6789"
  ]
}
```
| Parameter | Description                                            |
|-----------|--------------------------------------------------------|
| team      | **Required** The id or alias of the team being updated |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |

### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "neucode",
  "members": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }, {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "taiyang",
    "email_address": "taiyang@neucode.co",
    "email_address_confirmed": true
  }]
}
```
{% endmethod %}
