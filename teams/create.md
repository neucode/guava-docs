# Creating teams

{% method %}
## Creating teams {#post}

Create a team and add yourself as a member. You must be logged in. The alias
must be unique.

{% sample lang="http" %}
### Request
```
POST /api/teams
```
```
{
  "alias": "neucode"
}
```
| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "neucode",
  "members": [{
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true
  }]
}
```
{% endmethod %}
