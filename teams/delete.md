# Deleting teams

{% method %}
## Creating teams {#delete}

Delete a team. You must be a member of the team.

{% sample lang="http" %}
### Request
```
DELETE /api/teams/:team
```
| Parameter | Description                                            |
|-----------|--------------------------------------------------------|
| team      | **Required** The id or alias of the team being deleted |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
{% endmethod %}
