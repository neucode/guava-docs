# Streams

Streams are collections of tasks that have been grouped together with a common
topic. Streams are usually identified by an alias, which must be made up of
lowercase alphanumeric characters and dashes.

By default all users have an `inbox` stream. This stream cannot be deleted or
updated and cannot explicitly have tasks added to it.

Only one of `user` or `team` will be available, based on whether the stream 
is owned by a user or a team.

```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing",
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  },
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}
```

| Field      | Type   | Description                        |
|------------|--------|------------------------------------|
| created_at | date   | The date of creation               |
| updated_at | date   | The latest date of update          |
| id         | number | A unique numerical identifier      |
| alias      | string | A unique human-readable identifier |
| user       | object | The owning user                    |
| team       | object | The owning team                    |
