# Updating streams

{% method %}
## Updating personal streams {#patch}

Update a stream owned by you. You must be logged in.

{% sample lang="http" %}
### Request
```
PATCH /api/streams/:stream
```
```
{
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing"
}
```
| Parameter | Description                                              |
|-----------|----------------------------------------------------------|
| stream    | **Required** The id or alias of the stream being updated |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing",
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  }
}
```
{% endmethod %}

{% method %}
## Updating team streams {#patch}

Update a stream owned by the given team. You must be a member of the team.

{% sample lang="http" %}
### Request
```
PATCH /api/teams/:team/streams/:stream
```
```
{
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing"
}
```
| Parameter | Description                                              |
|-----------|----------------------------------------------------------|
| team      | **Required** The id or alias of the owning team          |
| stream    | **Required** The id or alias of the stream being updated |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing",
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}
```
{% endmethod %}
