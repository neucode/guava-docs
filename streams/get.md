# Getting streams

{% method %}
## Getting personal streams {#get}

Get all of your personal streams. You must be logged in.

{% sample lang="http" %}
### Request
```
GET /api/streams
```
| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
[{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing",
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  }
}]
```
{% endmethod %}

{% method %}
## Getting team streams {#get}

Get all of the streams owned by the given team. You must be a member of the
team.

{% sample lang="http" %}
### Request
```
GET /api/teams/:team/streams
```
| Parameter | Description                                     |
|-----------|-------------------------------------------------|
| team      | **Required** The id or alias of the owning team |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
[{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing",
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}]
```
{% endmethod %}

{% method %}
## Getting a personal stream {#get}

Get one of your personal streams. You must be logged in.

{% sample lang="http" %}
### Request
```
GET /api/streams/:stream
```
| Parameter | Description                                |
|-----------|--------------------------------------------|
| stream    | **Required** The id or alias of the stream |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing",
  "user": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "username": "benjamin",
    "email_address": "benjamin@neucode.co",
    "email_address_confirmed": true,
  }
}
```
{% endmethod %}

{% method %}
## Getting a team stream {#get}

Get one of the team streams. You must be a member of the team.

{% sample lang="http" %}
### Request
```
GET /api/teams/:team/streams/:stream
```
| Parameter | Description                                     |
|-----------|-------------------------------------------------|
| team      | **Required** The id or alias of the owning team |
| stream    | **Required** The id or alias of the stream      |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "alias": "marketing",
  "team": {
    "created_at": "2016-11-24T01:00:00+11:00",
    "updated_at": "2016-11-24T01:00:00+11:00",
    "id": "abcd-1234-wxyz-6789",
    "alias": "neucode",
    "members": [
      "abcd-1234-wxyz-6789"
    ]
  }
}
```
{% endmethod %}
