# Deleting streams

{% method %}
## Deleting personal streams {#delete}

Delete a stream owned you. You must be logged in.

{% sample lang="http" %}
### Request
```
DELETE /api/streams/:stream
```
| Parameter | Description                                              |
|-----------|----------------------------------------------------------|
| stream    | **Required** The id or alias of the stream being deleted |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |

{% endmethod %}

{% method %}
## Deleting team streams {#delete}

Delete a stream owned by the given team. You must be a member of the team.

{% sample lang="http" %}
### Request
```
DELETE /api/teams/:team/streams/:stream
```
| Parameter | Description                                              |
|-----------|----------------------------------------------------------|
| team      | **Required** The id or alias of the owning team          |
| stream    | **Required** The id or alias of the stream being deleted |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
{% endmethod %}
