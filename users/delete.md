# Deleting users

{% method %}
## Delete yourself {#delete}

Delete your account. You must be logged in.

{% sample lang="http" %}
### Request
```
DELETE /api/users/me
```
| Parameter | Description                     |
|-----------|---------------------------------|
| user      | **Required** The id of the user |

| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |
{% endmethod %}
