# Users

```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "username": "benjamin",
  "email_address": "benjamin@neucode.co",
  "email_address_confirmed": true,
}
```

| Field                   | Type    | Description                        |
|-------------------------|---------|------------------------------------|
| created_at              | date    | The date of creation               |
| updated_at              | date    | The latest date of update          |
| id                      | number  | A unique numerical identifier      |
| username                | string  | A unique human-readable identifier |
| email_address           | string  | All the users in this team         |
| email_address_confirmed | boolean | All the users in this team         |
