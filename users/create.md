# Creating users

{% method %}
## Creating users {#post}

Create a user. The username and email address must be unique. The user will
receive an email that they can use to verify their email address.

{% sample lang="http" %}
### Request
```
POST /api/users
```
```
{
  "username": "benjamin",
  "email_address": "benjamin@neucode.co",
  "password": "password"
}
```

### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "username": "loongy",
  "email_address": "benjamin@neucode.co",
  "email_address_confirmed": false,
}
```
{% endmethod %}
