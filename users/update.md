# Updating users

{% method %}
## Updating yourself {#patch}

Update yourself. You must be logged in.

{% sample lang="http" %}
### Request
```
PATCH /api/users/me
```
```
{
  "id": "abcd-1234-wxyz-6789",
  "username": "benjamin",
  "password": "password"
}
```
| Query | Description                            |
|-------|----------------------------------------|
| token | **Required** The JWT for authorization |

| Header  | Description                                    |
|---------|------------------------------------------------|
| Cookie  | **Required** `token` The JWT for authorization |

### Response
```
{
  "created_at": "2016-11-24T01:00:00+11:00",
  "updated_at": "2016-11-24T01:00:00+11:00",
  "id": "abcd-1234-wxyz-6789",
  "username": "benjamin",
  "email_address": "benjamin@neucode.co",
  "email_address_confirmed": true,
}
```
{% endmethod %}
